# Mypostman

In this project I have tried to cover the both API's REST & SOAP API. In the REST API, I have used the POST, PUT,PATCH,GET,DELETE methods for enabling the communication between the software applicatons. I have mentioned the details about importing the collecion & environments.It includes the data driven testing to ensure that the applicaton works foe the possible (fixed data)data.I have also used the Request chaining method to define the flow or sequence in which API's should be executed.


## Getting started

Anyone who wants to use this frameworks can clone & implement their respective schenarios. 
software needed: 
1) For development : Postman(latest version preferred)(download link :https://www.postman.com/downloads/)
2) For execution : Node.js, Newman.(download link :https://learning.postman.com/docs/collections/using-newman-cli/installing-running-newman/ )


## Git Commands to clone & pull 
git cone 
git pull



Postman Version I have used:

 Postman v11.1.0


## Project status
Work in progress 
I will be adding the more examples of APIs.


## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
